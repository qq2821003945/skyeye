# skyeye

> 智能制造一体化，采用Springboot + winUI的低代码平台开发模式。包含30多个应用模块、50多种电子流程，CRM、PM、ERP、MES、ADM、EHR、笔记、知识库、项目、门店、商城、财务、多班次考勤、薪资、招聘、云售后、论坛、公告、问卷、报表设计、工作流、日程、云盘等全面管理，实现智能制造行业一体化管理。实现管理流程“客户关系->
线上/线下报价->销售报价->销售合同->生产计划->商品设计->采购->加工制造->入库->发货->售后服务”的高效运作，同时实现企业员工的管理以及内部运作的流程操作，完善了员工从“入职->培训->转正->办公->离职”等多项功能。

- [2023年全年更新计划](https://mp.weixin.qq.com/s/deBkHLLeo1JDy6nqhvtWZg)
- [软件更新历史](https://gitee.com/doc_wei01/skyeye/blob/company_server/HISTORY_UPDATE.md)
- 开源版请下载`master`分支
- 接定制开发，无需求的不接
- 具备**低代码**、**快捷开发**、**可视化设计**、**微服务**等特点
- Skyeye 云系列功能导图：[地址](https://www.zhixi.com/view/9be57741)
- [Skyeye云系列资料](https://docs.qq.com/doc/DYUxuT3pSdGhVVXFC)
- **接前端 / Java后端等课程设计以及毕设制作，有需要的可加下方微信**

### 沟通交流

|   知识星球   |   作者微信   |          QQ群      |   Java学习 知识星球  |
|:-----:|:--------------------------------:|:--------------------:|:-------------------------------------:|
| ![](images/mindMap/知识星球.png) |    ![](images/mindMap/chatgpt的微信.jpg)    | ![](images/mindMap/Skyeye智能制造云办公官方①群群二维码.png) | ![输入图片说明](images/Java%E5%AD%A6%E4%B9%A0%E7%9F%A5%E8%AF%86%E6%98%9F%E7%90%83.png) |


#### 技术选型

##### 后端技术:

|技术|名称| 官网                                                       |
|---|---|----------------------------------------------------------|
|SpringBoot|核心框架| http://spring.io/projects/spring-boot                    |
|MyBatis|ORM框架| http://www.mybatis.org/mybatis-3/zh/index.html           |
|Druid|数据库连接池| https://github.com/alibaba/druid                         |
|Maven|项目构建管理| http://maven.apache.org/                                 |
|redis|key-value存储系统| https://redis.io/                                        |
|webSocket|浏览器与服务器全双工(full-duplex)通信| http://www.runoob.com/html/html5-websocket.html          |
|Flowable|工作流引擎| https://www.flowable.com/open-source/                    |
|xxl-job|定时任务| https://gitee.com/xuxueli0323/xxl-job?_from=gitee_search/ |
|RocketMQ|消息队列| https://rocketmq.apache.org/dowloading/releases/         |
|solr|企业级搜索应用服务器| https://lucene.apache.org/solr/                          |
|Spring Cloud|微服务框架(目前用户APP端接口)| https://springcloud.cc/                                  |

##### 前端技术：

|技术|名称| 官网                                       |
|---|---|------------------------------------------|
|layui|模块化前端UI| https://www.layui.com/                   |
|winui|win10风格UI| https://gitee.com/doc_wei01_admin/skyeye |

#### 效果图

| 效果图                                    | 效果图                                |
|----------------------------------------|------------------------------------|
| ![](images/show/tradition/show001.png) | ![](images/show/win10/show001.png) |
| ![](images/show/tradition/show002.png) | ![](images/show/win10/show002.png) |
| ![](images/show/tradition/show003.png) | ![](images/show/win10/show003.png) |
| ![](images/show/tradition/show004.png) | ![](images/show/win10/show004.png) |
| ![](images/show/tradition/show005.png) | ![](images/show/win10/show005.png) |
| ![](images/show/tradition/show006.png) | ![](images/show/win10/show006.png) |
| ![](images/show/tradition/show007.png) | ![](images/show/win10/show007.png) |
| ![](images/show/tradition/show008.png) | ![](images/show/win10/show008.png) |
| ![](images/show/tradition/show009.png) | ![](images/show/win10/show009.png) |
| ![](images/show/tradition/show010.png) | ![](images/show/win10/show010.png) |
